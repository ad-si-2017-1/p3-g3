//Faz requisição dos módulos necessários
var amqp = require('amqplib/callback_api'),
Telegram = require('node-telegram-bot-api');

var users = {}; // Usuários TLG
var amqp_conn;
var amqp_ch;
var telegram_client;


inicializar(); //cria uma nova instacia do Telegram com o token que ja foi gerado 

//inicia conexao com o amqp
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;
    amqp_getMessage(); // CONFIGURA CANAL DE RECEPÇÃO DAS MENSAGENS
	});
});

function inicializar () {
  const token = ''; //tem que gerar o token pelo @botFather ******* EM TEORIA, FALTA ESSE TOKEN ***********
  _bot = new Telegram(token, {polling: true});
}

_bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  var mensagem ={};
  mensagem.id = msg.chat.id;
  mensagem.nome = msg.chat.first_name + " " + msg.chat.last_name;
  mensagem.texto = msg.text;
  enviarParaCliente(JSON.stringify(mensagem));
  var envia = 'Você disse: ' + msg.text;
  _bot.sendMessage(chatId, envia);
});

function enviarParaCliente (msg) {
	msg = new Buffer(JSON.stringify(msg));
	amqp_ch.assertQueue("tl", {durable: false});
	amqp_ch.sendToQueue("tl", msg);
	console.log(" [telegram] Sent %s", msg);
}

// FUNCOES AUXILIARES PARA ENVIO E RECEBIMENTO AMQP
// ENVIA MENSAGEM PARA O CANAL ProxyTLG
function amqp_setMessage (id, msg)
{
	msg = new Buffer(JSON.stringify(msg));

	amqp_ch.assertQueue("ProxyTLG", {durable: true});
	amqp_ch.sendToQueue("ProxyTLG", msg);
	console.log("ProxyTLG Sent %s", msg);
}

// ProxyTLG É CONSUMIDOR CANAL WebChannel E PRODUTOR CANAL ProxyTLG
// RECEBE MENSAGENS DO CANAL WebChannel
function amqp_getMessage()
{
	amqp_ch.assertQueue ("WebChannel", {durable: true});
    amqp_ch.consume     ("WebChannel",
        function(msg)
        {
          console.log("WebChannel: Received %s", msg.content.toString());
          processaMensagem(JSON.parse(msg.content.toString()));
        }, {noAck: true});

    console.log("ProxyTLG: Waiting for messages from WebChannel");
}

// FUNCAO : ANALISA JSON ENVIADO PELO CANAL WebChannel
// ENTRADA: MENSAGEM JSON
//   SAIDA: ENVIO DADOS PARA SERVIDOR IRC E MENSAGENS PARA O CANAL ProxyTLG
function processaMensagem (dadosCliente)
{
    var servico  = dadosCliente.SERVICO;
    var comando  = dadosCliente.COMANDO;
    var userid   = dadosCliente.USERID;
    var nick     = dadosCliente.NICK;
    var passwd   = dadosCliente.PASSWD;
    var canal    = dadosCliente.CANAL;
    var msgWeb   = dadosCliente.MSG;
    var servidor = dadosCliente.SERVIDOR; //"irc.freenode.net";
   

    //1. AVALIA SE A MENSAGEM FOI DIRECIONADA AO ProxyTLG
    if (servico != "TLG") {
        console.log ("SERVICO DIRECIONADO A OUTRO PROXY: " + servico);
        return;
    }

    //2. AVALIA COMANDO ENVIADO

    // NEWUSR
    // CONFIGURAR CLIENTE IRC PARA RECEBER:
    //   MENSAGENS DO CANAL, ERRO E MOTD
    if (comando == "NEWUSR") 
    { 
        console.log ("ProxyTLG: NEWUSR SERVIDOR:"+ servidor + " NICK:" + nick + " CANAL:" + canal);

        telegram_client    = new irc.Client (servidor, nick, {channels: [canal]});

        telegram_client.addListener ('message'+canal,
           function (from, message) {
              var TD = new Date();
              var dInfo = TD.getDate() + ' ' + getHours() + ':' + getMinutes() +
                          ' [' + from + '] => ' + JSON.stringify(message);
              msg = {
                  "USERID"   : userid,
                  "TIMESTAMP": Date.now(),
                  "RESULT"   : "MENSAGEM SERVIDOR IRC",
                  "MURALIRC" : dInfo };

              amqp_setMessage (userid, msg);
              console.log(from + ' => '+ canal +': ' + message);
           });

        telegram_client.addListener ('error',
           function (message) {
              console.log('ERRO CANAL IRC: ', JSON.stringify(message));
              msg = {
                "USERID"   : userid,
                "TIMESTAMP": Date.now(),
                "MURALIRC" : "ERRO SERVIDOR IRC: " + JSON.stringify(message) };
              amqp_setMessage (userid, msg);
           });

        telegram_client.addListener ('motd',
           function (message) {
              var res = message.replace ("\\n", '\n');

              msg = {
                "USERID"   : userid,
                "TIMESTAMP": Date.now(),
                "RESULT"   : "MENSAGEM SERVIDOR IRC",
                "MURALIRC" : JSON.stringify(res) };
              amqp_setMessage (userid, msg);
           });

        users[userid] = telegram_client;

        msg = {
            "USERID"   : userid,
            "TIMESTAMP": Date.now(),
            "RESULT"   : "ACESSO SERVIDOR IRC CONFIGURADO COM SUCESSO!",
            "MURALIRC" : "INICIO TRANSMISSAO SALA [" + canal + "]" };
       amqp_setMessage (userid, msg);
    }
   
    // SETMSG
    // ENVIA MENSAGEM PARA O CANAL IRC
    if (comando == "SETMSG")
    {
        telegram_client = users[userid];
		telegram_client.say (canal,msgWeb);
        msg = {
            "USERID"   : userid,
            "TIMESTAMP": Date.now(),
            "RESULT"   : "MENSAGEM IRC [" + msgWeb + "] ENVIADA COM SUCESSO!"};
       amqp_setMessage (userid, msg);
	}

    // QUIT
    // FINALIZA CLIENTE IRC E REMOVE USUARIO
    if (comando == "QUIT")
    {
        telegram_client = users[userid];
        telegram_client.send ("quit");
        telegram_client.disconnect();

        msg = {
            "USERID"   : userid,
            "TIMESTAMP": Date.now(),
            "RESULT"   : "MENSAGEM IRC CLIENTE FINALIZADO" };
       amqp_setMessage (userid, msg);
       delete users[userid];
       delete telegram_client;
    }
}